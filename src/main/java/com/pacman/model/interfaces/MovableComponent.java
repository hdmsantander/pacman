package main.java.com.pacman.model.interfaces;

public interface MovableComponent {
	
	boolean canMoveLeft();
	
	void moveLeft();

	boolean canMoveRight();
	
	void moveRight();
	
	boolean canMoveUp();
	
	void moveUp();
	
	boolean canMoveDown();
	
	void moveDown();
	
	void move();

}
