package main.java.com.pacman.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import javax.imageio.ImageIO;

import main.java.com.pacman.presentation.GamePanel;
import main.java.com.pacman.services.SearchService;

public class Board extends Thread {
	
	private static final int BIN_WIDTH = 21;
	
	private static final int NO_MOVE =-1;
	
	private GamePanel gamePanel;
	
	private BufferedImage gameImage;
	
	private String backgroundPath;
	
	private ComponentPosition pacman;
	
	private ComponentPosition nodesAsComponents[];
	
	private double distanceMatrix[][];
	
	private ArrayList<FoodNode> foodNodes;
		
	private int boardMatrix[][] = {
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
			{1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
			{1,0,1,1,0,1,1,1,0,1,0,1,1,1,0,1,1,0,1},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
			{1,0,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,0,1},
			{1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1},
			{1,1,1,1,0,1,1,1,0,1,0,1,1,1,0,1,1,1,1},
			{1,1,1,1,0,1,0,0,0,0,0,0,0,1,0,1,1,1,1},
			{1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,1},
			{0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0},
			{1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,1},
			{1,1,1,1,0,1,0,0,0,0,0,0,0,1,0,1,1,1,1},
			{1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,1},
			{1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1},
			{1,0,1,1,0,1,1,1,0,1,0,1,1,1,0,1,1,0,1},
			{1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1},
			{1,1,0,1,0,1,0,1,1,1,1,1,0,1,0,1,0,1,1},
			{1,0,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,0,1},
			{1,0,1,1,1,1,1,1,0,1,0,1,1,1,1,1,1,0,1},
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
			};
	
	private int adjMatrix[][];
	
	public Board(GamePanel gamePanel) {
		this.gamePanel = gamePanel;
		this.backgroundPath = "background-cropped.jpg";
		this.pacman = new ComponentPosition(168,231);
		this.nodesAsComponents = new ComponentPosition[185];
		this.foodNodes = new ArrayList<>();
		this.gameImage = initializeBackgroundImage();
		this.adjMatrix = initializeAdjMatrix();
	}
	
	public void updatePacmanPosition(int x, int y) {
		this.pacman.x = x;
		this.pacman.y = y;
	}
	
	public boolean isValidPosition(int x, int y) {
		for(ComponentPosition component : nodesAsComponents) {
			if (component.getXPixels() == x && component.getYPixels() == y) {
				return true;
			}
		}
		return false;
	}
	
	public void paint (Graphics g) {
		paintBackground(g);
		paintFood(g);
	}
	
	public ComponentPosition[] getListOfNodes() {
		return nodesAsComponents;
	}
	
	public int getPinkysScaredMovement(int sourceX, int sourceY) {
		int destNode = translatePositionToNodeNo(21, 21);
		return getMovementToGetToNode(sourceX, sourceY, destNode);
	}

	public int getInkysScaredMovement(int sourceX, int sourceY) {
		int destNode = translatePositionToNodeNo(21, 21*19);
		return getMovementToGetToNode(sourceX, sourceY, destNode);
	}
	
	public int getClydesScaredMovement(int sourceX, int sourceY) {
		int destNode = translatePositionToNodeNo(21*17, 21*19);
		return getMovementToGetToNode(sourceX, sourceY, destNode);
	}
	
	public int getBlinkysScaredMovement(int sourceX, int sourceY) {
		int destNode = translatePositionToNodeNo(21*17, 21);
		return getMovementToGetToNode(sourceX, sourceY, destNode);
	}
	
	public int getMovementToGetToNode(int sourceX, int sourceY, int destinationNode) {
		
		int sourceNode = translatePositionToNodeNo(sourceX, sourceY);
		
		if (sourceNode == destinationNode) {
			return NO_MOVE;
		}
		
		int nodeToMoveTo = SearchService.getNodeChoiceToGetToGoal(adjMatrix, nodesAsComponents, sourceNode, destinationNode, false);
		
		if (nodeToMoveTo == -1) {
			return NO_MOVE;
		}
		
		int binnedX = Math.floorDiv(sourceX, BIN_WIDTH);
		int binnedY = Math.floorDiv(sourceY, BIN_WIDTH);
				
		if (nodesAsComponents[nodeToMoveTo].x == binnedX + 1 && nodesAsComponents[nodeToMoveTo].y == binnedY) {
			return KeyEvent.VK_RIGHT;
		}
		
		if (nodesAsComponents[nodeToMoveTo].x == binnedX -1 && nodesAsComponents[nodeToMoveTo].y == binnedY) {
			return KeyEvent.VK_LEFT;
		}
		
		if (nodesAsComponents[nodeToMoveTo].x == binnedX && nodesAsComponents[nodeToMoveTo].y == binnedY -1) {
			return KeyEvent.VK_UP;
		}
		
		if (nodesAsComponents[nodeToMoveTo].x == binnedX && nodesAsComponents[nodeToMoveTo].y == binnedY+1) {
			return KeyEvent.VK_DOWN;
		}
		
		return NO_MOVE;
		
	}
	
	public int getNextMovementToGetToPacman(int sourceX, int sourceY, boolean useEuclideanNorm, boolean isInky) {
								
		int sourceNode = translatePositionToNodeNo(sourceX, sourceY);
		int destNode = translatePositionToNodeNo(pacman.x, pacman.y);
		
		if (sourceNode == destNode) {
			return NO_MOVE;
		}
		
		int nodeToMoveTo = isInky ? SearchService.getInkysBestMove(adjMatrix, sourceNode, destNode) : SearchService.getNodeChoiceToGetToGoal(adjMatrix, nodesAsComponents, sourceNode, destNode, useEuclideanNorm);
		
		if (nodeToMoveTo == -1) {
			return NO_MOVE;
		}
			
		int binnedX = Math.floorDiv(sourceX, BIN_WIDTH);
		int binnedY = Math.floorDiv(sourceY, BIN_WIDTH);
				
		if (nodesAsComponents[nodeToMoveTo].x == binnedX + 1 && nodesAsComponents[nodeToMoveTo].y == binnedY) {
			return KeyEvent.VK_RIGHT;
		}
		
		if (nodesAsComponents[nodeToMoveTo].x == binnedX -1 && nodesAsComponents[nodeToMoveTo].y == binnedY) {
			return KeyEvent.VK_LEFT;
		}
		
		if (nodesAsComponents[nodeToMoveTo].x == binnedX && nodesAsComponents[nodeToMoveTo].y == binnedY -1) {
			return KeyEvent.VK_UP;
		}
		
		if (nodesAsComponents[nodeToMoveTo].x == binnedX && nodesAsComponents[nodeToMoveTo].y == binnedY+1) {
			return KeyEvent.VK_DOWN;
		}
		
		return NO_MOVE;
		
	}
	
	private int translatePositionToNodeNo(int x, int y) {
		
		int binnedX = Math.floorDiv(x, BIN_WIDTH);
		int binnedY = Math.floorDiv(y, BIN_WIDTH);
				
		for (int i = 0; i < 185 ; i++) {
			if (nodesAsComponents[i].x == binnedX && nodesAsComponents[i].y == binnedY) {
				return i;
			}
		}
		
		return -1;		
	}
	
	private int[][] initializeAdjMatrix() {
		
		// Sabemos que debe haber 185 nodos
		int a[][] = new int[185][185];
		
		// Inicializa la matriz de costos inicial con ceros
		for (int i = 0 ; i < a.length ; i++) {
			Arrays.fill(a[i], 0);
		}
		
		// Ya que estamos haciendo eso, vamos a inicializar la de distancias
		this.distanceMatrix = new double[185][185];
		
		// Inicializa la matriz de costos heuristicos con unos
		for (int i = 0 ; i < this.distanceMatrix.length ; i++) {
			Arrays.fill(this.distanceMatrix[i], 0.0);
		}
		
		int node = 0;
		
		// Recorremos la matriz de nodos
		for (int i=0; i < boardMatrix.length ; i++) {
			for (int j = 0 ; j < boardMatrix[0].length;j++) {
				
				// Si es una posición válida
				if (boardMatrix[i][j] == 0) {
																				
					// Revisando izquierda
					if (node -1 >= 0 && j-1 > 0 && boardMatrix[i][j-1]==0) {
						a[node][node-1]=1;
					}
					
					// Revisando derecha
					if(node+1< a.length && j+1 < boardMatrix[0].length && boardMatrix[i][j+1]==0) {
						a[node][node+1]=1;
					}
					
					// Revisando abajo
					int numNodoAbajo = getNumberOfNodeDown(i,j);
					
					if (numNodoAbajo!=-1) {
						a[node][numNodoAbajo]=1;
					}
					
					// Revisando arriba
					int numNodoArriba = getNumberOfNodeUp(i,j);
					
					if (numNodoArriba!=-1) {
						a[node][numNodoArriba]=1;
					}
					
					// Pasamos al siguiente nodo
					a[node][node]=0;
					nodesAsComponents[node] = new ComponentPosition(j, i);
					
					if(isPowerUp(j, i)) {
						foodNodes.add(new FoodNode(j*21+5,i*21+5,true));
					} else {
						foodNodes.add(new FoodNode(j*21+9,i*21+9,false));
					}
					node++;
				}
			}
		}
		
		// Ahora en base a la matrix de adyacencias, hacemos la heurística
		for (int i=0; i < a.length ; i++) {
			for (int j = 0 ; j < a[0].length;j++) {
				
				if (a[i][j]==1) {
					this.distanceMatrix[i][j]=Math.sqrt(Math.pow(nodesAsComponents[i].getXPixels()-nodesAsComponents[j].getXPixels(), 2) + Math.pow(nodesAsComponents[i].getYPixels()-nodesAsComponents[j].getYPixels(), 2));
				}
					
			}
		}
		
		return a;
	}
	
	private int getNumberOfNodeDown(int x, int y) {
		
		int nodeNo = -1;
		
		if (x + 1 >= boardMatrix.length) {
			
			return nodeNo;
			
		} else {
			
			// Count the number of nodes up to down of node at x,y
			for (int i=0; i < boardMatrix.length ; i++) {
				for (int j = 0 ; j < boardMatrix[0].length;j++) {
					if (boardMatrix[i][j]==0)
						nodeNo++;
					if (i == x + 1 && j == y) {
						if (boardMatrix[i][j]==0)
							return nodeNo;
						else
							return -1;
					}
				}
			}
			
			return nodeNo;
		}
	}
	
	private int getNumberOfNodeUp(int x, int y) {
		
		int nodeNo = -1;
		
		if (x - 1 <= 0) {
			
			return nodeNo;
			
		} else {
			
			// Count the number of nodes up to up of node at x,y
			for (int i=0; i < boardMatrix.length ; i++) {
				for (int j = 0; j < boardMatrix[0].length;j++) {
					if (boardMatrix[i][j]==0)
						nodeNo++;
					if (i == x - 1 && j == y) {
						if (boardMatrix[i][j]==0)
							return nodeNo;
						else
							return -1;
					}
				}
			}
			
			return nodeNo;
		}
	}
	
	@SuppressWarnings("unused")
	private void paintGrid(Graphics g) {
		int sizeSquares = 21;
		g.setColor(Color.CYAN);
		for (int i=0; i < boardMatrix[0].length ; i++) {
			for (int j = 0 ; j < boardMatrix.length;j++) {
				g.drawRect(i*sizeSquares,j*sizeSquares, sizeSquares, sizeSquares);
				if (boardMatrix[j][i]==0) {
					g.fillOval(i*sizeSquares,j*sizeSquares, 4, 4);
				}else {
					g.fillRect(i*sizeSquares,j*sizeSquares, 4, 4);
				}
			}
		}
	}
	
	private void paintFood(Graphics g) {
		
		// Iterate over the food
		for (Iterator<FoodNode> iterator = foodNodes.iterator() ; iterator.hasNext();) {
			FoodNode food = iterator.next();
			
			// Since we have to iterate over the food, might as well check if it's been eaten
			if (food.getBounds().intersects(gamePanel.getPacman().getBounds())) {
				if (food.isPowerUp)
					gamePanel.pacmanAtePowerUp();
				else
					gamePanel.pacmanAteBolita();
				iterator.remove();
			}
			
			// Paint the food
			food.paint(g);
		}
		
	}
	
	@SuppressWarnings("unused")
	private void printMatrix(int[][] matrix) {
		for (int i=0; i < matrix.length ; i++) {
			for (int j = 0 ; j < matrix[0].length;j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println(" ");
		}
	}
	
	@SuppressWarnings("unused")
	private void printMatrix(double[][] matrix) {
		for (int i=0; i < 185 ; i++) {
			for (int j = 0 ; j < 185;j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println(" ");
		}
	}
	
	private void paintBackground(Graphics g) {
		g.drawImage(gameImage, 0, 0, gamePanel);
	}
	
	private BufferedImage initializeBackgroundImage() {
		try {
			return ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(backgroundPath));
		} catch (IOException e) {
			return null;
		}
	}
	
	private boolean isPowerUp(int x, int y) {
		
		if (x == 1 && y == 2)
			return true;
		
		if (x == 1 && y == 18)
			return true;
		
		if (x == 17 && y == 2)
			return true;
		
		if (x==17 && y == 18)
			return true;
		
		return false;
	}
		
	public class ComponentPosition {
		int x;
		int y;
		public ComponentPosition(int x, int y) {
			this.x = x;
			this.y = y;
		}
		public int getXPixels() {
			return x*21;
		}
		public int getYPixels() {
			return y*21;
		}
	}
	
	private class FoodNode {
		int x;
		int y;
		boolean isPowerUp;
		public FoodNode(int x, int y, boolean isPowerUp) {
			this.x = x;
			this.y = y;
			this.isPowerUp = isPowerUp;
		}
		public Rectangle getBounds() {
			return new Rectangle(x,y,4,4);
		}
		public void paint(Graphics g) {
			g.setColor(Color.YELLOW);
			if (isPowerUp)
				g.fillOval(x,y,10,10);
			else
				g.fillOval(x,y,4,4);
		}
	}
}
