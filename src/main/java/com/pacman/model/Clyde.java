package main.java.com.pacman.model;

import java.awt.event.KeyEvent;
import java.util.Random;

import main.java.com.pacman.model.enums.GhostName;
import main.java.com.pacman.model.interfaces.MovableComponent;
import main.java.com.pacman.presentation.GamePanel;

public class Clyde extends Ghost implements MovableComponent{
	
	private static final GhostName GHOST_NAME = GhostName.CLYDE;
	private static final String SPRITE_PATH = "clyde-cropped.png";

	private int currentMove;
	private int previousMove;
	
	private Random random;
	
	public Clyde(GamePanel gamePanel, Board board, int x, int y) {
		super(GHOST_NAME, gamePanel, board, SPRITE_PATH, x, y);
		this.random = new Random();
		this.currentMove = getRandomMovement();
		this.previousMove = currentMove;
	}
	
	@Override
	public void run() {
		update();
	}
	
	private void update() {
		while(active) {
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
			if (!wasEaten)
				move();
			else
				doJailSecuence();
		}
	}
	
	@Override
	public void move() {
		
		if (this.isScared ) {
			switch (board.getClydesScaredMovement(x, y)) {

			case KeyEvent.VK_LEFT:
				if (canMoveLeft()) {
					moveLeft();
				}
				break;

			case KeyEvent.VK_RIGHT:
				if (canMoveRight()) {
					moveRight();
				} 
				break;

			case KeyEvent.VK_UP:
				if (canMoveUp()) {
					moveUp();
				}
				break;

			case KeyEvent.VK_DOWN:
				if (canMoveDown()) {
					moveDown();
				}
				break;
			default:
				
				break;
			}
			
		} else {
			boolean foundMovement = false;
			int nextMov = previousMove;
					
			while(!foundMovement) {
				
				switch (currentMove) {

				case KeyEvent.VK_LEFT:
					if (canMoveLeft()) {
						moveLeft();
						foundMovement=true;
					} else {
						while(nextMov == previousMove || nextMov == currentMove) {
							nextMov = getRandomMovement();
						}
						previousMove = currentMove;
						currentMove = nextMov;
					}
					break;

				case KeyEvent.VK_RIGHT:
					if (canMoveRight()) {
						moveRight();
						foundMovement=true;
					}  else {
						while(nextMov == previousMove || nextMov == currentMove) {
							nextMov = getRandomMovement();
						}
						previousMove = currentMove;
						currentMove = nextMov;
					}
					break;

				case KeyEvent.VK_UP:
					if (canMoveUp()) {
						moveUp();
						foundMovement=true;
					}  else {
						while(nextMov == previousMove || nextMov == currentMove) {
							nextMov = getRandomMovement();
						}
						previousMove = currentMove;
						currentMove = nextMov;
					}
					break;

				case KeyEvent.VK_DOWN:
					if (canMoveDown()) {
						moveDown();
						foundMovement=true;
					}  else {
						while(nextMov == previousMove || nextMov == currentMove) {
							nextMov = getRandomMovement();
						}
						previousMove = currentMove;
						currentMove = nextMov;
					}
					break;
				default:
					
					break;
				}
			}
		}
		
	}

	@Override
	public boolean canMoveLeft() {
		return board.isValidPosition(x-getStepWidth(), y);
	}

	@Override
	public void moveLeft() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				x-=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
		
	}

	@Override
	public boolean canMoveRight() {
		return board.isValidPosition(x+getStepWidth(), y);
	}

	@Override
	public void moveRight() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				x+=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
		
	}

	@Override
	public boolean canMoveUp() {
		return board.isValidPosition(x, y-getStepWidth());
	}

	@Override
	public void moveUp() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				y-=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
		
	}

	@Override
	public boolean canMoveDown() {
		return board.isValidPosition(x, y+getStepWidth());
	}

	@Override
	public void moveDown() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				y+=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
	}
	
	private int getRandomMovement() {
		int choice = random.nextInt() % 4;
			switch(choice) {
			
			case 0:
				return KeyEvent.VK_RIGHT;
				
			case 1:
				return KeyEvent.VK_LEFT;
				
			case 2:
				return KeyEvent.VK_UP;
				
			case 3:
				return KeyEvent.VK_DOWN;
			
				default:
					return KeyEvent.VK_UP;		
			}
	}
	
	@Override
	public void doJailSecuence() {

		// Move ghost to jail
		this.x = 210;
		this.y = 189;
		
		this.isScared = false;
		
		waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		
		for (int i = 0 ; i < 5 ; i ++) {
			moveLeft();
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
			moveLeft();
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
			moveRight();
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
			moveRight();
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		}
		

		moveLeft();
		waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		moveUp();
		waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		moveUp();
		waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		
		wasEaten = false;
		
	}

}
