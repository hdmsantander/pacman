package main.java.com.pacman.model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import main.java.com.pacman.model.interfaces.MovableComponent;
import main.java.com.pacman.presentation.GamePanel;

public class Pacman extends Thread implements MovableComponent{
	
	private int refreshSpeedLongs = 20;
	private int refreshSpeedNanos = 0;
	
	private static final int STEP_WIDTH = 21;
	
	private boolean active;
	
	private int x;
	private int y;
	
	private int currentMove;
	private int previousMove;
	
	private GamePanel gamePanel;
	
	private Board board;
	
	public Pacman(GamePanel gamePanel, Board board, String spriteImagePath, int x, int y) {
		this.gamePanel = gamePanel;
		this.board = board;
		this.x = x;
		this.y = y;
		this.active = true;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x,y,21,21);
	}
		
	public void paint(Graphics g) {
		g.setColor(Color.YELLOW);
		g.fillOval(x, y, 21, 21);
	}
	
	@Override
	public void run() {
		update();
	}
	
	// Toma la dirección actual de movimiento y realiza el movimiento si puede
	// hacerlo
	public void move() {

		switch (currentMove) {

		case KeyEvent.VK_LEFT:
			if (canMoveLeft()) {
				moveLeft();
			}
			break;

		case KeyEvent.VK_RIGHT:
			if (canMoveRight()) {
				moveRight();
			}
			break;

		case KeyEvent.VK_UP:
			if (canMoveUp()) {
				moveUp();
			}
			break;

		case KeyEvent.VK_DOWN:
			if (canMoveDown()) {
				moveDown();
			}
			break;
		default:
			break;

		}
	}
	
	// Método que recibe un código de tecla de gamePanel para ajustar la dirección
	public void setMovingDirection(int keyCode) {
		previousMove = currentMove;
		currentMove = keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_RIGHT || keyCode == KeyEvent.VK_DOWN
				|| keyCode == KeyEvent.VK_UP ? keyCode : currentMove;
	}
	
	private void update() {
		while(active) {
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
			move();
			board.updatePacmanPosition(x, y);
		}
	}
	
	private void waitSomeTime(long milis, int nanos) {
		try {
			if (this.isAlive())
				Thread.sleep(milis, nanos);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	

	@Override
	public boolean canMoveLeft() {
		return board.isValidPosition(x-STEP_WIDTH, y);
	}

	@Override
	public void moveLeft() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				x-=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
	}

	@Override
	public boolean canMoveRight() {
		return board.isValidPosition(x+STEP_WIDTH, y);
	}

	@Override
	public void moveRight() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				x+=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
	}

	@Override
	public boolean canMoveUp() {
		return board.isValidPosition(x, y-STEP_WIDTH);
	}

	@Override
	public void moveUp() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				y-=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
	}

	@Override
	public boolean canMoveDown() {
		return board.isValidPosition(x, y+STEP_WIDTH);
	}

	@Override
	public void moveDown() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				y+=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
	}

}
