package main.java.com.pacman.model;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.java.com.pacman.model.enums.GhostName;
import main.java.com.pacman.presentation.GamePanel;

public abstract class Ghost extends Thread {
	
	public int refreshSpeedLongs = 30;
	public int refreshSpeedNanos = 0;
	
	private static final int STEP_WIDTH = 21;
	
	private static final String SCARED_SPRITE = "asustado-cropped.png";
	
	@SuppressWarnings("unused")
	private GhostName name;
	
	public boolean active;
	
	public boolean isScared;
	
	public boolean wasEaten;
		
	public int x;
	public int y;
	
	public GamePanel gamePanel;
	
	public Board board;
		
	private String spriteImagePath;
	
	public Ghost(GhostName name, GamePanel gamePanel, Board board, String spriteImagePath, int x, int y) {
		this.name = name;
		this.gamePanel = gamePanel;
		this.board = board;
		this.spriteImagePath = spriteImagePath;
		this.x = x;
		this.y = y;
		this.isScared = false;
		this.wasEaten = false;
		this.active = true;
	}
			
	public void paint(Graphics g) {
		paintSprite(g);
	}
	
	private void paintSprite(Graphics g) {
		try {
			if (!isScared)
				g.drawImage(ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(spriteImagePath)), x, y, gamePanel);
			else
				g.drawImage(ImageIO.read(GamePanel.class.getClassLoader().getResourceAsStream(SCARED_SPRITE)), x, y, gamePanel);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void waitSomeTime(long milis, int nanos) {
		try {
			if (this.isAlive())
				Thread.sleep(milis, nanos);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public int getStepWidth() {
		return STEP_WIDTH;
	}
	
	public void hasBeenEaten() {
		this.wasEaten = true;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, 21,21);
	}
	
	public abstract void doJailSecuence();
		
}
