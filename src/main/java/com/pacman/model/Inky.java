package main.java.com.pacman.model;

import java.awt.event.KeyEvent;
import main.java.com.pacman.model.enums.GhostName;
import main.java.com.pacman.model.interfaces.MovableComponent;
import main.java.com.pacman.presentation.GamePanel;

public class Inky extends Ghost implements MovableComponent {
	
	private static final GhostName GHOST_NAME = GhostName.INKY;
	private static final String SPRITE_PATH = "inky-cropped.png";
	
	public Inky(GamePanel gamePanel, Board board , int x, int y) {
		super(GHOST_NAME, gamePanel, board, SPRITE_PATH, x, y);
	}
	
	@Override
	public void run() {
		update();
	}
	
	private void update() {
		while(active) {
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
			if (!wasEaten)
				move();
			else
				doJailSecuence();
		}
	}
	
	@Override
	public boolean canMoveLeft() {
		return board.isValidPosition(x-getStepWidth(), y);
	}

	@Override
	public void moveLeft() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				x-=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
		
	}

	@Override
	public boolean canMoveRight() {
		return board.isValidPosition(x+getStepWidth(), y);
	}

	@Override
	public void moveRight() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				x+=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
		
	}

	@Override
	public boolean canMoveUp() {
		return board.isValidPosition(x, y-getStepWidth());
	}

	@Override
	public void moveUp() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				y-=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
		
	}

	@Override
	public boolean canMoveDown() {
		return board.isValidPosition(x, y+getStepWidth());
	}

	@Override
	public void moveDown() {
		for (int i = 0; i < 3 ; i++) {
			for (int j = 0 ; j < 7 ; j++) {
				y+=1;
				waitSomeTime(refreshSpeedLongs,0);
			}
		}
	}


	@Override
	public void move() {
		
		int proposedMove = this.isScared ? board.getInkysScaredMovement(x, y) : board.getNextMovementToGetToPacman(x, y, false,true);
				
		switch (proposedMove) {

		case KeyEvent.VK_LEFT:
			if (canMoveLeft()) {
				moveLeft();
			}
			break;

		case KeyEvent.VK_RIGHT:
			if (canMoveRight()) {
				moveRight();
			}
			break;

		case KeyEvent.VK_UP:
			if (canMoveUp()) {
				moveUp();
			}
			break;

		case KeyEvent.VK_DOWN:
			if (canMoveDown()) {
				moveDown();
			}
			break;
		default:
			// Skip a turn
			waitSomeTime(refreshSpeedLongs,0);
			break;
		}
	}

	@Override
	public void doJailSecuence() {
				
		// Move ghost to jail
		this.x = 210;
		this.y = 189;
		
		this.isScared = false;
		
		waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		
		for (int i = 0 ; i < 5 ; i ++) {
			moveLeft();
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
			moveLeft();
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
			moveRight();
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
			moveRight();
			waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		}
		

		moveLeft();
		waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		moveUp();
		waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		moveUp();
		waitSomeTime(refreshSpeedLongs, refreshSpeedNanos);
		
		wasEaten = false;
		
	}

}
