package main.java.com.pacman.model.enums;

public enum GhostName {
	BLINKY, CLYDE, INKY, PINKY
}
