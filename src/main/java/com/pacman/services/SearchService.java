package main.java.com.pacman.services;

import java.util.Arrays;
import java.util.Random;

import main.java.com.pacman.model.Board.ComponentPosition;

public class SearchService {

	// This class is accessed statically, hence private constructor
	private SearchService() {}

	// Constant to signal that a node doesn't have a parent
	private static final int NO_PARENT = -1;
	
	private static final Random RN = new Random();

	// This method gets the choice of movement that is optimal to get to the
	// destination node
	public static int getNodeChoiceToGetToGoal(int[][] graph, ComponentPosition[] components, int start, int goal,
			boolean useEuclideanNorm) {

		// If we're already there, just send it
		if (start == goal)
			return start;

		// Initialize parents to the number of nodes
		int parents[] = new int[graph[start].length];

		// Start node doesn't have a parent
		parents[start] = NO_PARENT;

		// This contains the distances from the start node to all other nodes
		int[] distances = new int[graph.length];

		// Initializing with a distance of "Infinity"
		Arrays.fill(distances, Integer.MAX_VALUE);

		// The distance from the start node to itself is of course 0
		distances[start] = 0;

		// This contains the priorities with which to visit the nodes, calculated using
		// the heuristic.
		double[] priorities = new double[graph.length];

		// Initializing with a priority of "Infinity"
		Arrays.fill(priorities, Integer.MAX_VALUE);

		// Start node has a priority equal to straight line distance to goal. It will be
		// the first to be expanded.
		priorities[start] = useEuclideanNorm ? getEuclideanNorm(components[start], components[goal])
				: getNumberOfNodesToGoal(graph, components, start, goal);

		// This contains whether a node was already visited
		boolean[] visited = new boolean[graph.length];

		// While nodes left
		while (true) {

			// Find the current lowest priority
			double lowestPriority = Integer.MAX_VALUE;
			int lowestPriorityIndex = -1;

			for (int i = 0; i < priorities.length; i++) {

				// All priorities that have lower priority
				if (priorities[i] < lowestPriority && !visited[i]) {
					lowestPriority = priorities[i];
					lowestPriorityIndex = i;
				}

			}

			if (lowestPriorityIndex == -1) {
				// We failed finding path
				return -1;

				// We're there
			} else if (lowestPriorityIndex == goal) {
				return getBestMove(goal, start, parents);
			}

			// Check all neighboring nodes
			for (int i = 0; i < graph[lowestPriorityIndex].length; i++) {

				if (graph[lowestPriorityIndex][i] != 0 && !visited[i]) {

					// Check shorter path
					if (distances[lowestPriorityIndex] + graph[lowestPriorityIndex][i] < distances[i]) {
						parents[i] = lowestPriorityIndex;
						distances[i] = distances[lowestPriorityIndex] + graph[lowestPriorityIndex][i];
						priorities[i] = useEuclideanNorm
								? distances[i] + getEuclideanNorm(components[lowestPriorityIndex], components[i])
								: distances[i] + getNumberOfNodesToGoal(graph, components, lowestPriorityIndex, i);
					}
				}
			}

			// We're done with this node
			visited[lowestPriorityIndex] = true;
		}

	}

	// This method counts the number of nodes to the target node goal
	private static int getNumberOfNodesToGoal(int[][] graph, ComponentPosition[] components, int start, int goal) {

		// If we're already there, just send it
		if (start == goal)
			return 1;

		// Initialize parents to the number of nodes
		int parents[] = new int[graph[start].length];

		// Start node doesn't have a parent
		parents[start] = NO_PARENT;

		// This contains the distances from the start node to all other nodes
		int[] distances = new int[graph.length];

		// Initializing with a distance of "Infinity"
		Arrays.fill(distances, Integer.MAX_VALUE);

		// The distance from the start node to itself is of course 0
		distances[start] = 0;

		// This contains the priorities with which to visit the nodes, calculated using
		// the heuristic.
		double[] priorities = new double[graph.length];

		// Initializing with a priority of "Infinity"
		Arrays.fill(priorities, Integer.MAX_VALUE);

		// Start node has a priority equal to straight line distance to goal. It will be
		// the first to be expanded.
		priorities[start] = getEuclideanNorm(components[start], components[goal]);

		// This contains whether a node was already visited
		boolean[] visited = new boolean[graph.length];

		// While nodes left
		while (true) {

			// Find the current lowest priority
			double lowestPriority = Integer.MAX_VALUE;
			int lowestPriorityIndex = -1;

			for (int i = 0; i < priorities.length; i++) {

				// All priorities that have lower priority
				if (priorities[i] < lowestPriority && !visited[i]) {
					lowestPriority = priorities[i];
					lowestPriorityIndex = i;
				}

			}

			if (lowestPriorityIndex == -1) {
				// We failed finding path, asume is like, really far, man.
				return graph[0].length;

				// We're there
			} else if (lowestPriorityIndex == goal) {
				return getNumberOfNodes(goal, start, parents, 0);
			}

			// Check all neighboring nodes
			for (int i = 0; i < graph[lowestPriorityIndex].length; i++) {

				if (graph[lowestPriorityIndex][i] != 0 && !visited[i]) {

					// Check shorter path
					if (distances[lowestPriorityIndex] + graph[lowestPriorityIndex][i] < distances[i]) {
						parents[i] = lowestPriorityIndex;
						distances[i] = distances[lowestPriorityIndex] + graph[lowestPriorityIndex][i];
						priorities[i] = distances[i] + getEuclideanNorm(components[start], components[goal]);
					}
				}
			}

			// We're done with this node
			visited[lowestPriorityIndex] = true;
		}

	}
	
	// Not so good implementation of star without heuristics
	public static int getInkysBestMove(int[][] board, int start, int goal) {
		
		// Record the num of vertices
		int numOfVertices = board[0].length;

		// Initialize the shortest distances
		int[] shortestDistances = new int[numOfVertices];
		
		// Initialize visited nodes
		boolean[] visited = new boolean[numOfVertices];
		
		// Fill both arrays
		for (int vertexIndex = 0; vertexIndex < numOfVertices; vertexIndex++) {
			shortestDistances[vertexIndex] = Integer.MAX_VALUE;
			visited[vertexIndex] = false;
		}
		
		// Distance from start to start is 0
		shortestDistances[start] = 0;
		
		// This is to keep track of parents
		int[] parents = new int[numOfVertices];
		
		// First node doesn't have a parent
		parents[start] = NO_PARENT;
		
		// Explore
		for (int i = 1; i < numOfVertices; i++) {

			int nearestVertex = -1;
			int shortestDistance = Integer.MAX_VALUE;

			for (int vertexIndex = 0; vertexIndex < numOfVertices; vertexIndex++) {
				if (!visited[vertexIndex] && shortestDistances[vertexIndex] < shortestDistance) {
					nearestVertex = vertexIndex;
					shortestDistance = shortestDistances[vertexIndex];
				}
			}

			visited[nearestVertex] = true;

			for (int vertexIndex = 0; vertexIndex < numOfVertices; vertexIndex++) {

				int edgeDistance = board[nearestVertex][vertexIndex];
				if (edgeDistance > 0 && ((shortestDistance + edgeDistance) < shortestDistances[vertexIndex])) {

					parents[vertexIndex] = nearestVertex;
					
					// Spice up the path finding to make it not predictable
					shortestDistances[vertexIndex] = shortestDistance + edgeDistance + RN.nextInt(board[0].length);

				}

			}

		}
		
		return getBestMove(goal,start,parents);

	}

	// Iterate over parents and return the node adjacent to the starting node
	// it's the best choice of movement to get to the goal
	private static int getBestMove(int currentNode, int desiredNode, int[] parents) {
		if (parents[currentNode] == desiredNode)
			return currentNode;

		return getBestMove(parents[currentNode], desiredNode, parents);
	}

	// Iterate over parents and return the number of nodes
	private static int getNumberOfNodes(int currentNode, int desiredNode, int[] parents, int cost) {

		if (parents[currentNode] == desiredNode)
			return cost;

		return getNumberOfNodes(parents[currentNode], desiredNode, parents, cost + 1);
	}

	// Returns the euclidean norm
	private static double getEuclideanNorm(ComponentPosition a, ComponentPosition b) {
		return Math.sqrt(Math.pow((double) a.getXPixels() - (double) b.getXPixels(), 2)
				+ Math.pow((double) a.getYPixels() - (double) b.getYPixels(), 2));
	}
}
