package main.java.com.pacman.presentation;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class MainMenuFrame extends JFrame{
	
	public static final int WIDTH = 300;
	public static final int HEIGHT = 500;

	private Dimension dimensions;

	private static final long serialVersionUID = -3307506219359464480L;
	
	private GameFrame gameFrame;
	private GamePanel gamePanel;

	private MainMenuPanel mainMenuPanel;

	public MainMenuFrame() {
		super();
		this.dimensions = new Dimension(WIDTH, HEIGHT);
		setMinimumSize(dimensions);
		setMaximumSize(dimensions);
		setPreferredSize(dimensions);
		setResizable(false);
		setTitle("~ M E N U ~");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.mainMenuPanel = new MainMenuPanel(this);
		add(mainMenuPanel);
		pack();
		setVisible(true);
	}
	
	public GameFrame getGameFrame() {
		return gameFrame;
	}
	
	public void startGame() {
		this.gameFrame = new GameFrame(this);
		this.gamePanel = new GamePanel(this.gameFrame);
		gameFrame.startGame();
	}
	
	public int getMaximumWidth() {
		return WIDTH;
	}
	
	public int getMaximumHeight() {
		return HEIGHT - 30;
	}
		
	public void invokeGame() {
		this.gameFrame.setVisible(true);
	}
	
}
