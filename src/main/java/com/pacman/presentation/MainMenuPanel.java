package main.java.com.pacman.presentation;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainMenuPanel extends JPanel{
	
	private MainMenuFrame mainMenuFrame;
	
	private JButton jbStartGame;
	private JButton jbExitGame;
	
	public MainMenuPanel(MainMenuFrame mainMenuFrame) {
		
		this.mainMenuFrame = mainMenuFrame;
		this.jbStartGame = new JButton("¡Iniciar juego!");
		this.jbExitGame = new JButton("Salir");
		add(jbStartGame);
		add(jbExitGame);
		
		jbStartGame.addActionListener(e -> {
			mainMenuFrame.setVisible(false);
			mainMenuFrame.startGame();
		});
		
		jbExitGame.addActionListener(e -> {
			mainMenuFrame.dispose();
		});

	}

}
