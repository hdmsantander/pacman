package main.java.com.pacman.presentation;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class GameFrame extends JFrame {

	public static final int WIDTH = 400;
	public static final int HEIGHT = 470;

	private Dimension dimensions;

	private static final long serialVersionUID = -3307506519359464480L;
	
	private MainMenuFrame mainMenuFrame;

	private GamePanel gamePanel = null;

	public GameFrame(MainMenuFrame mainMenuFrame) {
		super();
		this.dimensions = new Dimension(WIDTH, HEIGHT);
		setMinimumSize(dimensions);
		setMaximumSize(dimensions);
		setPreferredSize(dimensions);
		setResizable(false);
		setTitle("~ P A C M A N ~");
		setLocationRelativeTo(null);
		this.mainMenuFrame = mainMenuFrame;
		this.gamePanel = new GamePanel(this);
		add(gamePanel);
		pack();
		setVisible(false);
		addWindowListener(new WindowAdapter() {
		    @Override
		    public void windowClosing(WindowEvent e) {
		    mainMenuFrame.setVisible(true);
		    mainMenuFrame.toFront();
			dispose();
		    }
		});

	}
	
	public void startGame() {
		setVisible(true);
		gamePanel.startGame();
	}
	
	public int getMaximumWidth() {
		return WIDTH;
	}
	
	public int getMaximumHeight() {
		return HEIGHT - 30;
	}
}
