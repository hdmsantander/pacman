package main.java.com.pacman.presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import javax.swing.JPanel;
import javax.swing.Timer;

import main.java.com.pacman.model.Blinky;
import main.java.com.pacman.model.Board;
import main.java.com.pacman.model.Clyde;
import main.java.com.pacman.model.Ghost;
import main.java.com.pacman.model.Inky;
import main.java.com.pacman.model.Pacman;
import main.java.com.pacman.model.Pinky;


@SuppressWarnings("serial")
public class GamePanel extends JPanel implements ActionListener, KeyListener {
	
	private GameFrame gameFrame;
	
	private int playerScore = 0;
	
	private Board board;
	private List<Ghost> ghosts;
	private Pacman pacman;
	
	private Timer timer;
	
	public GamePanel(GameFrame gameFrame) {
		this.gameFrame = gameFrame;
		this.board = new Board(this);
		this.pacman = new Pacman(this,board,"pacman-cropped.png",189,231);
		this.ghosts = new ArrayList<>();
		this.ghosts.add(new Clyde(this,this.board,294,231));
		this.ghosts.add(new Pinky(this, this.board, 21,21));
		this.ghosts.add(new Blinky(this, this.board, 210, 21));
		this.ghosts.add(new Inky(this,this.board,21,399));
		this.timer = new Timer(1, this);
		this.addKeyListener(this);
		this.setFocusable(true);		
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		board.paint(g);
		pacman.paint(g);
		for (Ghost ghost : ghosts) {
			if (ghost.getBounds().intersects(pacman.getBounds())) {
				if (ghost.isScared) {
					pacmanAteGhost(ghost);
				} else {
					playerLost();
				}
			}
			ghost.paint(g);
			
		}
		
		g.setFont(new Font("default", Font.BOLD, 14));
		g.setColor(Color.WHITE);
		g.drawString("Puntuación Jugador: " + playerScore, 21, 15);
		
	}
	
	private void playerLost() {
		System.out.println("Player lost");
	}
		
	public Pacman getPacman() {
		return pacman;
	}
	
	public void startGame() {
		setVisible(true);
		timer.start();
		pacman.start();
		for (Ghost ghost : ghosts) {
			ghost.start();
		}
	}
	
	public void stopGame() {
		timer.stop();
		playerScore = 0;
		pacman.interrupt();
		for (Ghost ghost : ghosts) {
			ghost.interrupt();
		}
	}
	
	public void pacmanAteBolita() {
		playerScore++;
	}
	
	public void pacmanAtePowerUp() {
		
		playerScore+=10;
		
		for (Ghost ghost : ghosts) {
			ghost.isScared = true;
		}
		
		new java.util.Timer().schedule(new TimerTask(){
	        @Override
	        public void run() {
	            calmDownGhosts();
	        }
	    },10000,10000); 
		
	}
	
	private void calmDownGhosts() {
		for (Ghost ghost : ghosts) {
			ghost.isScared = false;
		}
	}
	
	public void pacmanAteGhost(Ghost ghost) {
		ghost.hasBeenEaten();
		ghost.x = 210;
		ghost.y = 189;
		playerScore+=20;
	}
	
	private void update() {
		if(this.isDisplayable() && this.isValid()) {
			repaint();
		}
	}
	
	public void keyPressed(KeyEvent event) {
		pacman.setMovingDirection(event.getKeyCode());
	}

	public void keyReleased(KeyEvent arg0) {
		
	}

	public void keyTyped(KeyEvent arg0) {
		
	}

	public void actionPerformed(ActionEvent arg0) {
		update();		
	}

}
